package codility;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SolutionCodilityTest {
  private SolutionCodility solution;

  @Before
  public void setup() {
    this.solution = new SolutionCodility();
  }

  @Test public void testGetSomeSingleString() {
    assertTrue("toTestOnly".equals(this.solution.getSomeSingleString()));
  }
}

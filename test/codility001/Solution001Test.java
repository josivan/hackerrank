package codility001;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class Solution001Test {
  private Solution001 s;
  private Random random;

  @Before
  public void setup() {
    this.s = new Solution001();
    this.random = new Random();
  }

  @Test
  public void testBlowInt() {
    int i = Integer.MAX_VALUE;
    long l = (long) i * 2;
    int[] x = { (int) l };
    System.out.println("tests");
  }

  @Test
  public void testSolution() {
    int[] data = { -1, 3, -4, 5, 1, -6, 2, 1 };
    System.out.println(s.solution(data));
    System.out.println(s.solution(new int[] { 1, 2, 3, 4, -1 }));

    int[] data2 = { 0, Integer.MAX_VALUE, 2, -2 };
    System.out.println(s.solution(data2));

    int[] data3 = { Integer.MIN_VALUE, Integer.MAX_VALUE, 2, -2 };
    System.out.println(s.solution(data3));

    // for (int i = 0; i < 10; i++) {
    // System.out.println(s.solution(random()));
    // }
  }

  private int[] random() {
    int size = this.random.nextInt(100001);
    int[] x = new int[size];

    for (int i = 0; i < size; i++) {
      x[i] = this.random.nextInt();
    }

    return x;
  }
}

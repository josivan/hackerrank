//https://www.hackerrank.com/contests/w21/challenges/kangaroo
package www.hackerrank.com.contests.w21.challenges.kangaroo;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      int x1 = sc.nextInt();
      int v1 = sc.nextInt();
      int x2 = sc.nextInt();
      int v2 = sc.nextInt();

      boolean isK1Ahead = Math.max(x1, x2) == x1;
      boolean isK1Faster = Math.max(v1, v2) == v1;

      boolean isK2Ahead = Math.max(x1, x2) == x2;
      boolean isK2Faster = Math.max(v1, v2) == v2;
      
      if ((isK1Ahead && isK1Faster) || (isK2Ahead && isK2Faster)) {
        System.out.println("NO");
      }
      else {
        boolean sameLocation = false;
        while (x1 < 10_000 && x2 < 10_000 && !sameLocation) {
          x1 += v1;
          x2 += v2;
          sameLocation = x1 == x2;
        }
        System.out.println(sameLocation ? "YES" : "NO");
      }
    }
  }

}

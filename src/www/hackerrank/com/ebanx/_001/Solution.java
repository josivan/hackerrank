package www.hackerrank.com.ebanx._001;

public class Solution {

  public static void main(String[] args) {
    System.out.println(mergeStrings("abc", "def"));
    System.out.println(mergeStrings("ab", "zsd"));
  }
  
  static String mergeStrings(String s1, String s2) {
    int l1 = s1.length();
    int l2 = s2.length();
    int maxLength = Math.max(l1, l2);
    
    StringBuilder result = new StringBuilder();
    
    for (int i = 0; i < maxLength; i++) {
      result.append(i >= l1 ? "" : Character.toString(s1.charAt(i)));
      result.append(i >= l2 ? "" : Character.toString(s2.charAt(i)));
    }
    
    return result.toString();
  }
}

package www.hackerrank.com.ebanx._002;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Solution {

  public static void main(String[] args) {
    int[] i1 = {3, 1, 2, 2, 4};
    int[] i2 = {8, 5, 5, 5, 5, 1, 1, 1, 4, 4};
  }
  
  static void handle(int[] data) {
    Arrays.sort(data);
    
    Map<Integer, Integer> counting = new HashMap<>();
    
    for (int d : data) {
      Integer count = counting.get(d);
      
      if (count == null) {
        counting.put(d, 1);
      }
      else {
        counting.put(d, ++count);
      }
    }
  }

}

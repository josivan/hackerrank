// https://www.hackerrank.com/challenges/sha-256
package www.hackerrank.com.challenges.sha._256;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      String s = sc.nextLine();
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      System.out.printf("%064x", new BigInteger(1, md.digest(s.getBytes())));
    }
    catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
  }

}

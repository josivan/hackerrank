package www.hackerrank.com.challenges.algorithms.warmup;

import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class TimeConversion {

    /*
     * Complete the timeConversion function below.
     */
    static String timeConversion(String s) {
        /*
         * Write your code here.
         */
      String[] input = s.split(":");
      int h = Integer.parseInt(input[0]);
      if (input[2].endsWith("PM")) {
        h += (h > 11 ? 0 : 12);
      }
      else {
        h = (h > 11 ? 0 : h);
      }
      input[0] = String.format("%02d", h); 
      input[2] = input[2].substring(0, 2);
      return String.join(":", input);
    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
      System.out.println(timeConversion("07:05:45PM"));
      System.out.println(timeConversion("10:05:45AM"));
      System.out.println(timeConversion("3:05:45AM"));
      System.out.println(timeConversion("12:45:54PM"));
      /*
        BufferedWriter bw = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = scan.nextLine();

        String result = timeConversion(s);

        bw.write(result);
        bw.newLine();

        bw.close();
        */
    }
}
// https://www.hackerrank.com/challenges/compare-the-triplets/problem
package www.hackerrank.com.challenges.algorithms.warmup;

import java.util.Scanner;

/*
1 2 3 4 5 => 10 14
140537896 243908675 670291834 923018467 520718469 => 1575456874 2357937445
*/
public class CompareTheTriplets {

  // Complete the miniMaxSum function below.
  static void miniMaxSum(int[] arr) {
    long max = Long.MIN_VALUE;
    long min = Long.MAX_VALUE;

    for (int i = 0; i < arr.length; i++) {
      long sum = 0;
      for (int j = 0; j < arr.length; j++) {
        if (j != i) {
          sum += arr[j];
        }
      }
      max = Math.max(max, sum);
      min = Math.min(min, sum);
    }

    System.out.print(min + " " + max);
  }

  private static final Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {
    int[] arr = new int[5];

    String[] arrItems = scanner.nextLine().split(" ");
    // scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

    for (int i = 0; i < 5; i++) {
      int arrItem = Integer.parseInt(arrItems[i]);
      arr[i] = arrItem;
    }

    miniMaxSum(arr);

    scanner.close();
  }
}
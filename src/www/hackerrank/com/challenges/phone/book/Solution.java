//https://www.hackerrank.com/challenges/phone-book?h_r=internal-search
package www.hackerrank.com.challenges.phone.book;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Solution {
  public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
      int n = in.nextInt();
      in.nextLine();

      Map<String, Integer> data = new HashMap<>(n);

      for (int i = 0; i < n; i++) {
        String name = in.next();
        int phone = in.nextInt();
        data.put(name, Integer.valueOf(phone));
        in.nextLine();
      }

      while (in.hasNext()) {
        String s = in.nextLine();

        if (data.containsKey(s)) {
          System.out.println(s + "=" + data.get(s));
          continue;
        }

        System.out.println("Not found");
      }
    }
  }
}

//https://www.hackerrank.com/challenges/30-binary-numbers
package www.hackerrank.com.challenges._30.binary.numbers;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      int data = sc.nextInt();
      char[] asBinary = Integer.toBinaryString(data).toCharArray();
      
      int count = 0;
      int max = 0;

      for (int i = 0, size = asBinary.length; i < size; i++) {
        if (asBinary[i] == '1') {
          max = Math.max(max, ++count);
        }
        else {
          count = 0;
        }
      }
      
      System.out.println(max);
    } 
  }

}

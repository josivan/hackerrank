//https://www.hackerrank.com/challenges/30-conditional-statements
package www.hackerrank.com.challenges._30.conditional.statements;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner scan = new Scanner(System.in)) {
      int n = scan.nextInt();
      scan.close();
      String ans = "";

      // if 'n' is NOT evenly divisible by 2 (i.e.: n is odd)
      if (n % 2 == 1) {
        ans = "Weird";
      }
      else {
        if (n > 1 && n < 5) {
          ans = "Not Weird";
        }
        else if (n > 5 && n < 21) {
          ans = "Weird";
        }
        else {
          ans = "Not Weird";
        }
      }
      System.out.println(ans);
    }
  }
}

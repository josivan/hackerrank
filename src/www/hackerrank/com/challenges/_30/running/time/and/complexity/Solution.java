//https://www.hackerrank.com/challenges/30-running-time-and-complexity
package www.hackerrank.com.challenges._30.running.time.and.complexity;

import java.math.BigInteger;
import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
      int op = in.nextInt();
      
      while (op > 0) {
        BigInteger n = in.nextBigInteger();
        System.out.println(n.isProbablePrime(1) ? "Prime" : "Not prime");
        op--;
      }
    }
  }

}

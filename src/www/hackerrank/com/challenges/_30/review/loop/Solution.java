//https://www.hackerrank.com/challenges/30-review-loop
package www.hackerrank.com.challenges._30.review.loop;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      int op = sc.nextInt();
      
      while (op > 0) {
        String s = sc.next();
        
        char[] data = s.toCharArray();

        printEven(data);
        System.out.print(' ');
        printOdd(data);
        System.out.println();
        
        op--;
      }
    }
  }

  static void printEven(char[] data) {
    for (int i = 0; i < data.length; i++) {
      if (i % 2 == 0) {
        System.out.print(data[i]);
      }
    }
  }
  
  static void printOdd(char[] data) {
    for (int i = 0; i < data.length; i++) {
      if (i % 2 == 1) {
        System.out.print(data[i]);
      }
    }
  }
}

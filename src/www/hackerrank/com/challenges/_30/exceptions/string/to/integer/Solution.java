//https://www.hackerrank.com/challenges/30-exceptions-string-to-integer
package www.hackerrank.com.challenges._30.exceptions.string.to.integer;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      String s = sc.next();
      
      System.out.println(Integer.parseInt(s));
    }
    catch (NumberFormatException nfe) {
      System.out.println("Bad String");
    }
  }

}

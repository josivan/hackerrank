//https://www.hackerrank.com/challenges/30-operators
package www.hackerrank.com.challenges._30.operators;

import java.util.Scanner;

public class Arithmetic {

  public static void main(String[] args) {
    try (Scanner scan = new Scanner(System.in)) {
      double mealCost = scan.nextDouble(); // original meal price
      int tipPercent = scan.nextInt(); // tip percentage
      int taxPercent = scan.nextInt(); // tax percentage
      scan.close();

      double tipValue = mealCost * (double) tipPercent / 100;
      double taxValue = mealCost * (double) taxPercent / 100;

      int totalCost = (int) Math.round((double) (tipValue + taxValue + mealCost));

      System.out.printf("The total meal cost is %d dollars.%n", totalCost);
    }
  }
}

//https://www.hackerrank.com/challenges/30-recursion
package www.hackerrank.com.challenges._30.recursion;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      int i = sc.nextInt();
      System.out.println(factorial(i));
    }

  }

  static int factorial(int i) {
    if (i == 1) {
      return 1;
    }
    
    return i * factorial(i - 1);
  }
}

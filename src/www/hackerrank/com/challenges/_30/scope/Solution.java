//https://www.hackerrank.com/challenges/30-scope
package www.hackerrank.com.challenges._30.scope;

import java.util.Scanner;

class Difference {
  private int[] elements;
  public int maximumDifference;

  Difference(int[] data) {
    this.elements = data;
  }

  void computeDifference() {
      this.maximumDifference = this.result(0, this.elements.length);
  }
  
  int result(int index, int size) {
    int result = Integer.MIN_VALUE;
    
    for (int i = index + 1; i < size; i++) {
      result = Math.max(result, Math.abs(this.elements[index] - this.elements[i]));
    }
    
    if (index < size) {
      result = Math.max(result, result(index + 1, size));
    }
    
    return result;
  }
}

public class Solution {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    int[] a = new int[n];
    for (int i = 0; i < n; i++) {
      a[i] = sc.nextInt();
    }
    sc.close();

    Difference difference = new Difference(a);

    difference.computeDifference();

    System.out.print(difference.maximumDifference);
  }
}
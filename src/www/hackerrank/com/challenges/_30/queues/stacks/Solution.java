//https://www.hackerrank.com/challenges/30-queues-stacks
package www.hackerrank.com.challenges._30.queues.stacks;

import java.util.Scanner;

public class Solution {
  private char[] queue = new char[0];
  private char[] stack = new char[0];
  
  void pushCharacter(char c) {
    this.queue = add(c, this.queue);
  }
  
  char popCharacter() {
    int length = this.queue.length;
    char result = this.queue[length - 1];
    char[] aux = new char[length - 1];
    System.arraycopy(queue, 0, aux, 0, length - 1);
    queue = aux;
    return result;
  }
  
  void enqueueCharacter(char c) {
    this.stack = add(c, this.stack);
  }
  
  char[] add(char c, char[] data) {
    int length = data.length;
    
    if (length == 0) {
      return new char[]{c};
    }
    
    char[] aux = new char[length + 1];
    System.arraycopy(data, 0, aux, 0, length);
    aux[length] = c;
    return aux;
  }

  char dequeueCharacter() {
    int length = this.stack.length;
    char result = this.stack[0];
    char[] aux = new char[length - 1];
    System.arraycopy(stack, 1, aux, 0, length - 1);
    stack = aux;
    return result;
  }
  
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    String input = scan.nextLine();
    scan.close();

    // Convert input String to an array of characters:
    char[] s = input.toCharArray();

    // Create a Solution object:
    Solution p = new Solution();

    // Enqueue/Push all chars to their respective data structures:
    for (char c : s) {
      p.pushCharacter(c);
      p.enqueueCharacter(c);
    }

    // Pop/Dequeue the chars at the head of both data structures and compare
    // them:
    boolean isPalindrome = true;
    for (int i = 0; i < s.length / 2; i++) {
      if (p.popCharacter() != p.dequeueCharacter()) {
        isPalindrome = false;
        break;
      }
    }

    // Finally, print whether string s is palindrome or not.
    System.out.println("The word, " + input + ", is " + ((!isPalindrome) ? "not a palindrome." : "a palindrome."));
  }
}

//https://www.hackerrank.com/challenges/diagonal-difference
package www.hackerrank.com.challenges.diagonal.difference;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      int n = sc.nextInt();

      int[][] data = new int[n][n];

      for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
          data[i][j] = sc.nextInt();
        }
      }
      
      int d1 = 0, d2 = 0;
      
      for (int i = 0; i < n; i++) {
        d1 += data[i][i]; 
      }
      
      for (int i = n - 1, j = 0; j < n; i--, j++) {
        d2 += data[i][j];
      }
      
      System.out.println(Math.abs(d1 - d2));
    }
  }

}

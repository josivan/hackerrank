//https://www.hackerrank.com/challenges/arrays-ds
package www.hackerrank.com.challenges.arrays.ds;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      int n = sc.nextInt();
      int[] data = new int[n];

      for (int i = 0; i < n; i++) {
        data[i] = sc.nextInt();
      }

      for (int i = 0, j = n - 1; i < j; i++, j--) {
        int aux = data[i];
        data[i] = data[j];
        data[j] = aux;
      }

      for (int i = 0; i < n; i++) {
        System.out.print(data[i] + " ");
      }
    }
  }

}

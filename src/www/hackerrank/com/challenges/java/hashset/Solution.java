//https://www.hackerrank.com/challenges/java-hashset
package www.hackerrank.com.challenges.java.hashset;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      int size = sc.nextInt();
      sc.nextLine();
      Set<String> set = new HashSet<>();

      for (int i = 0; i < size; i++) {
        String line = sc.nextLine();
        set.add(line);
        System.out.println(set.size());
      }
    }
  }
}
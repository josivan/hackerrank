//https://www.hackerrank.com/challenges/java-anagrams
package www.hackerrank.com.challenges.java.anagrams;

import java.util.Arrays;
import java.util.Scanner;

public class Solution {

  static boolean isAnagram(String a, String b) {
    char[] ca = a.toLowerCase().toCharArray();
    char[] cb = b.toLowerCase().toCharArray();
    Arrays.sort(ca);
    Arrays.sort(cb);

    return Arrays.toString(ca).equals(Arrays.toString(cb));

  }

  public static void main(String[] args) {

    Scanner scan = new Scanner(System.in);
    String a = scan.next();
    String b = scan.next();
    scan.close();
    boolean ret = isAnagram(a, b);
    System.out.println((ret) ? "Anagrams" : "Not Anagrams");
  }
}

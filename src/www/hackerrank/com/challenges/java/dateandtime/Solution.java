// https://www.hackerrank.com/challenges/java-date-and-time
package www.hackerrank.com.challenges.java.dateandtime;

import java.util.Calendar;
import java.util.Locale;
import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
      String month = in.next();
      String day = in.next();
      String year = in.next();
      checkYear(year);
      checkDay(month, day, year);
    }
  }
  
  private static void checkYear(String year) {
    int y = Integer.parseInt(year);
    
    if (y < 2000 || y > 3000) {
      throw new IllegalArgumentException("Year must be between 2000 and 3000");
    }
  }
  
  private static void checkDay(String... data) {
    Calendar calendar = Calendar.getInstance();
    calendar.set(
      Integer.parseInt(data[2]), 
      Integer.parseInt(data[0]) - 1, 
      Integer.parseInt(data[1]));
    
    System.out.println(calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.ENGLISH).toUpperCase());
  }
}

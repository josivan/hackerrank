//https://www.hackerrank.com/challenges/java-static-initializer-block
package www.hackerrank.com.challenges.java._static.initializer.block;

import java.util.Scanner;

public class Solution {
  static int B = 0;
  static int H = 0;
  static boolean flag = false;

  static {
    try (Scanner s = new Scanner(System.in)) {
      B = s.nextInt();
      H = s.nextInt();
      if (B <= 0 || H <= 0) {
        throw new Exception("Breadth and height must be positive");
      }
      flag = true;
    }
    catch (Exception e) {
      System.out.println(e);
      System.exit(-1);
    }
  }

  public static void main(String[] args) {
    if (flag) {
      int area = B * H;
      System.out.print(area);
    }

  }// end of main

}// end of class

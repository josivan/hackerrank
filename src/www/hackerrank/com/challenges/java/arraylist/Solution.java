//https://www.hackerrank.com/challenges/java-arraylist
package www.hackerrank.com.challenges.java.arraylist;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner s = new Scanner(System.in)) {
      int nLines = s.nextInt();
      s.nextLine();

      List<int[]> list = new ArrayList<>(nLines);

      while (nLines > 0) {
        int nElements = s.nextInt();
        int[] data = new int[nElements];

        for (int i = 0; i < nElements; i++) {
          data[i] = s.nextInt();
        }

        list.add(data);
        s.nextLine();

        nLines--;
      }

      int nOp = s.nextInt();
      s.nextLine();

      while (nOp > 0) {
        int l = s.nextInt();
        int c = s.nextInt();
        s.nextLine();

        int[] data = list.get(l - 1);

        if (data.length < c) {
          System.out.println("ERROR!");
        }
        else {
          System.out.println(data[c - 1]);
        }

        nOp--;
      }
    }
  }
}

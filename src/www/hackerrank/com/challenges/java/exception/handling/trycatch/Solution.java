//https://www.hackerrank.com/challenges/java-exception-handling-try-catch/submissions/code/22514187
package www.hackerrank.com.challenges.java.exception.handling.trycatch;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try {
      Scanner sc = new Scanner(System.in);
      int x = sc.nextInt();
      int y = sc.nextInt();
      int z = x / y;
      System.out.println(z);
    }
    catch (InputMismatchException e) {
      System.out.println(e.getClass().getName());
    }
    catch (Exception e) {
      System.out.println(e);
    }
  }

}

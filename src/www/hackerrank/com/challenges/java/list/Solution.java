//https://www.hackerrank.com/challenges/java-list
package www.hackerrank.com.challenges.java.list;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      int size = sc.nextInt();
      sc.nextLine();
      String array = sc.nextLine();
      int nOp = sc.nextInt();
      sc.nextLine();

      int[] data = new int[size];

      String[] values = array.split(" ");

      // for (int i = 0; i < size; i++) {
      // data[i] = Integer.parseInt(values[i]);
      // }

      List<String> asList = new LinkedList<>(Arrays.asList(values));
      while (nOp > 0) {
        String op = sc.nextLine();
        String p = sc.nextLine();

        if ("Delete".equals(op)) {
          asList.remove(Integer.parseInt(p));
        }
        else {
          String[] p2 = p.split(" ");
          asList.add(Integer.parseInt(p2[0]), p2[1]);
        }

        nOp--;
      }

      for (String s : asList) {
        System.out.print(s + " ");
      }
    }
  }

}

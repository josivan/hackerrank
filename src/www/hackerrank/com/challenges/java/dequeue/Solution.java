// https://www.hackerrank.com/challenges/java-dequeue
package www.hackerrank.com.challenges.java.dequeue;

import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Set;

/*
6 3
5 3 5 2 3 2

3 3
1 2 3
-> 3
 */
public class Solution {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    Deque<Integer> deque = new LinkedList<>();
    Set<Integer> set = new HashSet<>();
    int n = in.nextInt();
    int m = in.nextInt();
    
    long max = 0;
    
    for (int i = 0; i < n; i++) {
      int num = in.nextInt();
      deque.addLast(num);
      set.add(num);

      if (deque.size() == m) {
        max = Math.max(max, set.size());
        Integer value = deque.removeFirst();
        if (!deque.contains(value))
          set.remove(value);
      }
    }

    System.out.println(max);
  }
}
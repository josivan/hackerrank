//https://www.hackerrank.com/challenges/java-regex
package www.hackerrank.com.challenges.java.regex;

import java.util.Scanner;

public class Solution {
  public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
      while (in.hasNext()) {
        String IP = in.next();
        System.out.println(IP.matches(new myRegex().pattern));
      }
    }
  }
}

class myRegex {
  // String pattern = "(\\d{1,3}.){3}\\d{1,3}" ;
  String pattern = "([0-2]?[0-5]?[0-5]?\\.){3}([0-2]?[0-5]?[0-5]?)";
}
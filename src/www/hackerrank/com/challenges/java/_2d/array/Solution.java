//https://www.hackerrank.com/challenges/java-2d-array
package www.hackerrank.com.challenges.java._2d.array;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
      int arr[][] = new int[6][6];
      for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 6; j++) {
          arr[i][j] = in.nextInt();
        }
      }
      
      int maxSum = Integer.MIN_VALUE;

      for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
          maxSum = Math.max(maxSum, sum(subData(i, j, arr)));
        }
      }

      System.out.println(maxSum);
    }
  }

  static int[][] subData(int r, int c, int[][] data) {
    int[][] result = new int[3][3];
    for (int i = r, countL = 0; countL < 3; i++, countL++) {
      for (int j = c, countR = 0; countR < 3; j++, countR++) {
        result[countL][countR] = data[i][j];
      }
    }
    return result;
  }

  static int sum(int[][] data) {
    int sum = 0;
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        if (i == 1 && i != j)
          continue;
        sum += data[i][j];
      }
    }
    return sum;
  }

}

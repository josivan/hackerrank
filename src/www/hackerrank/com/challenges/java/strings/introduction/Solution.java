//https://www.hackerrank.com/challenges/java-strings-introduction?h_r=internal-search
package www.hackerrank.com.challenges.java.strings.introduction;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      String A = sc.next();
      String B = sc.next();
      int size = A.length() + B.length();
      int order = A.compareTo(B);

      System.out.println(size);
      System.out.println(order < 0 ? "Yes" : "No");
      String a = A.substring(0, 1).toUpperCase().concat(A.substring(1));
      String b = B.substring(0, 1).toUpperCase().concat(B.substring(1));
      System.out.printf("%s %s", a, b);
    }
  }

}

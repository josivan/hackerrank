//https://www.hackerrank.com/challenges/java-1d-array-easy
package www.hackerrank.com.challenges.java._1d.array.easy;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      int size = sc.nextInt();
      
      int[] data = new int[size];
      
      for (int i = 0; i < size; i++) {
        data[i] = sc.nextInt();
      }
      
      int count = 0;
      int sum = 0;

      for (int i = 0; i < size; i++) {
        for (int j = i; j < size; j++) {
          sum += data[j];
          if (sum < 0) {
            count++;
          }
        }
        sum = 0;
      }
      
      System.out.println(count);
    }
  }
}

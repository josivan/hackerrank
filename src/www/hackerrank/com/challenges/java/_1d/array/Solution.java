//https://www.hackerrank.com/challenges/java-1d-array
package www.hackerrank.com.challenges.java._1d.array;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      int tc = sc.nextInt();
      
      while (tc > 0) {
        int length = sc.nextInt();
        int jump = sc.nextInt();
        
        int[] data = new int[length];
        
        for (int i = 0; i < length; i++) {
          data[i] = sc.nextInt();
        }

        System.out.println(isPossibleToWin(0, data, length, jump) ? "YES" : "NO");
        
        tc--;
      }
    }
  }

  static boolean isPossibleToWin(int begin, int[] data, int size, int jump) {
    if (begin < 0 || data[begin] == 1) {
      return false;
    }
    
    if ((begin == size -1) || (begin + jump) > size - 1) {
      return true;
    }
    
    data[begin] = 1;
    
    return 
        isPossibleToWin(begin + 1, data, size, jump)
        || isPossibleToWin(begin - 1, data, size, jump)
        || isPossibleToWin(begin + jump, data, size, jump);
  }
}

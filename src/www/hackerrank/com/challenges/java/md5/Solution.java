// https://www.hackerrank.com/challenges/java-md5
package www.hackerrank.com.challenges.java.md5;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      String s = sc.nextLine();
      MessageDigest md = MessageDigest.getInstance("MD5");
      System.out.println(new BigInteger(1, md.digest(s.getBytes())).toString(16));
    }
    catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }    
  }

}

//https://www.hackerrank.com/challenges/java-string-compare?h_r=internal-search
package www.hackerrank.com.challenges.java.string.compare;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      String s = sc.next();
      int k = sc.nextInt();

      List<String> list = new ArrayList<>();

      for (int i = 0, l = s.length(); k <= l; i++, k++) {
        list.add(s.substring(i, k));
      }

      Collections.sort(list);

      System.out.println(list.get(0));
      System.out.println(list.get(list.size() - 1));
    }
  }

}

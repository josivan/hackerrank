//https://www.hackerrank.com/challenges/java-string-tokens
package www.hackerrank.com.challenges.java.string.tokens;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner scan = new Scanner(System.in)) {
      if (!scan.hasNext()) {
        System.out.print(0);
        return;
      }
      
      String s = scan.nextLine();
      scan.close();

      // Write your code here.
      String[] data = s.trim().split("[\\p{Punct}\\s]+");

      System.out.println(data.length);

      for (String s1 : data) {
        System.out.println(s1.length() == 0 ? "0" : s1);
      }
    }
  }

}

//https://www.hackerrank.com/challenges/java-string-reverse?h_r=internal-search
package www.hackerrank.com.challenges.java.string.reverse;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      String s = sc.next();

      String s2 = new StringBuilder(s).reverse().toString();

      System.out.println(s2.equalsIgnoreCase(s) ? "Yes" : "No");
    }
  }

}

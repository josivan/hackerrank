//https://www.hackerrank.com/challenges/java-sort
package www.hackerrank.com.challenges.java.sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

class Student {
  private int id;
  private String fname;
  private double cgpa;

  public Student(int id, String fname, double cgpa) {
    super();
    this.id = id;
    this.fname = fname;
    this.cgpa = cgpa;
  }

  public int getId() {
    return id;
  }

  public String getFname() {
    return fname;
  }

  public double getCgpa() {
    return cgpa;
  }
}

public class Solution {
  public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
      int testCases = Integer.parseInt(in.nextLine());

      List<Student> studentList = new ArrayList<Student>();
      while (testCases > 0) {
        int id = in.nextInt();
        String fname = in.next();
        double cgpa = in.nextDouble();

        Student st = new Student(id, fname, cgpa);
        studentList.add(st);

        testCases--;
      }

      Comparator<Student> comp = new Comparator<Student>() {
        @Override
        public int compare(Student o1, Student o2) {
          int result = 0;

          result = (int) ((o2.getCgpa() * 1000) - (o1.getCgpa() * 1000));

          if (result == 0) {
            result = o1.getFname().compareTo(o2.getFname());
          }

          if (result == 0) {
            result = o1.getId() - o2.getId();
          }

          return result;
        }
      };

      Collections.sort(studentList, comp);

      for (Student st : studentList) {
        System.out.println(st.getFname());
      }
    }
  }
}

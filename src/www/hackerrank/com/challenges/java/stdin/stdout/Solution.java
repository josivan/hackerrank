//https://www.hackerrank.com/challenges/java-stdin-stdout/submissions/code/22389633
package www.hackerrank.com.challenges.java.stdin.stdout;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      int x = sc.nextInt();
      sc.nextLine();
      double y = sc.nextDouble();
      sc.nextLine();
      String s = sc.nextLine();

      System.out.println("String: " + s);
      System.out.println("Double: " + y);
      System.out.println("Int: " + x);
    }
  }
}
//https://www.hackerrank.com/challenges/java-biginteger
package www.hackerrank.com.challenges.java.biginteger;

import java.math.BigInteger;
import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      String s1 = sc.nextLine();
      String s2 = sc.nextLine();

      BigInteger bi1 = new BigInteger(s1);
      BigInteger bi2 = new BigInteger(s2);

      System.out.println(bi1.add(bi2));
      System.out.println(bi1.multiply(bi2));
    }
  }

}

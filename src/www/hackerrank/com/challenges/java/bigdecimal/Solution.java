//https://www.hackerrank.com/challenges/java-bigdecimal
package www.hackerrank.com.challenges.java.bigdecimal;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Solution {

  public static void main(String[] argh) {
    try (Scanner sc = new Scanner(System.in)) {
      int n = sc.nextInt();
      String[] s = new String[n];
      for (int i = 0; i < n; i++) {
        s[i] = sc.next();
      }

      // Write your code here
      Arrays.sort(s, new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
          return new BigDecimal(o2).compareTo(new BigDecimal(o1));
        }
      });

      // Output
      for (int i = 0; i < n; i++) {
        System.out.println(s[i]);
      }
    }
  }

}

//https://www.hackerrank.com/challenges/java-loops-i
package www.hackerrank.com.challenges.java.loops.i;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      int input = sc.nextInt();
      
      if (input < 2 || input > 20) {
        throw new IllegalArgumentException("The value must be between 2 and 20.");
      }
      
      for (int i = 1; i < 11; i++) 
        System.out.printf("%d x %d = %d%n", input, i, (i * input));
    }
  }
}

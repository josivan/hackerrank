//https://www.hackerrank.com/challenges/java-loops
package www.hackerrank.com.challenges.java.loops;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      int t = sc.nextInt();
      sc.nextLine();

      while (t > 0) {
        int a = sc.nextInt();
        int b = sc.nextInt();
        int n = sc.nextInt();

        int r = a;
        for (int exp = 0; exp < n; exp++) {
          r += Math.pow(2, exp) * b;
          System.out.print(r + " ");
        }
        System.out.println("");
        t--;
      }
    }
  }

}
//https://www.hackerrank.com/challenges/java-covariance
package www.hackerrank.com.challenges.java.covariance;

class Flower {
  public String whats_Your_Name() {
    return "I have many name and type";
  }
}

class Lotus extends Flower {
  @Override public String whats_Your_Name() {
    return "Lotus";
  }
}

class Lily extends Flower {
  @Override public String whats_Your_Name() {
    return "Lily";
  }
}

class Jasmine extends Flower {
  @Override public String whats_Your_Name() {
    return "Jasmine";
  }
}

class State {
  Flower your_National_Flower() {
    return new Flower();
  }
}

class WestBengal extends State {
  @Override Jasmine your_National_Flower() {
    return new Jasmine();
  }
}

class Karnataka extends State {
  @Override Lotus your_National_Flower() {
    return new Lotus();
  }
}

class AndhraPradesh extends State {
  @Override Lily your_National_Flower() {
    return new Lily();
  }
}

public class Solution {

  public static void main(String[] args) {
    // TODO Auto-generated method stub

  }

}

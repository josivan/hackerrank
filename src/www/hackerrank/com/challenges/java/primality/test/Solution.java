//https://www.hackerrank.com/challenges/java-primality-test
package www.hackerrank.com.challenges.java.primality.test;

import java.math.BigInteger;
import java.util.Scanner;

public class Solution {
  public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
      BigInteger n = in.nextBigInteger();
      System.out.println(n.isProbablePrime(1) ? "prime" : "not prime");
    }
  }
}

// https://www.hackerrank.com/challenges/staircase
package www.hackerrank.com.challenges.staircase;

import java.util.Scanner;

public class Solution {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      int size = sc.nextInt();
      
      for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
          System.out.print((j + i) >= (size - 1) ? '#' : ' ');
        }
        System.out.println();
      }
    }
  }

}

// https://www.hackerrank.com/challenges/ctci-array-left-rotation/problem
package www.hackerrank.com.challenges.ctci.datastructures.arrays;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class LeftLocation {

  // Complete the rotLeft function below.
  static int[] rotLeft(int[] a, int d) {
    for (int i = 0, j = 0; j < d; j++) {
      int temp = a[i];
      
      int k = i;
      for (; k < a.length - 1;) {
        a[k] = a[++k];
      }
      
      a[k] = temp;
    }
    
    return a;
  }

  private static final Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) throws IOException {
    /*
    int n = 5;
    int d = 4;
    int[] a = new int[] {1, 2, 3, 4, 5};
    int[] result = rotLeft(a, d);
    
    for (int i = 0; i < result.length; i++) {
      System.out.print(i + " ");
    }*/
    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

    String[] nd = scanner.nextLine().split(" ");

    int n = Integer.parseInt(nd[0]);

    int d = Integer.parseInt(nd[1]);

    int[] a = new int[n];

    String[] aItems = scanner.nextLine().split(" ");
    scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

    for (int i = 0; i < n; i++) {
      int aItem = Integer.parseInt(aItems[i]);
      a[i] = aItem;
    }

    int[] result = rotLeft(a, d);

    for (int i = 0; i < result.length; i++) {
      bufferedWriter.write(String.valueOf(result[i]));

      if (i != result.length - 1) {
        bufferedWriter.write(" ");
      }
    }

    bufferedWriter.newLine();

    bufferedWriter.close();

    scanner.close();
  }
}

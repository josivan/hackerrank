// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");
package codility001;

public class Solution001 {
  public int solution(int[] A) {
    if (A.length == 0) {
      return -1;
    }

    long sum = 0;

    for (int i = 0; i < A.length; i++) {
      sum += (long) A[i];
    }

    long sumLeft = 0;

    for (int i = 0; i < A.length; i++) {
      long sumRight = sum - sumLeft - (long) A[i];

      if (sumLeft == sumRight) {
        return i;
      }

      sumLeft += (long) A[i];
    }

    return -1;
  }
}
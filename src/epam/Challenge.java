package epam;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Challenge {
  /*
  List<Integer> sourceList // {1,2,3,4}
  List<Integer> resultList = OBJ.insert(sourceList, 2, 8);
  resultList // {1,2,8,3,4}
  */

  /// somewhere in the OBJ class
  public static List<Integer> insert(List<Integer> sourceList, Integer position, Integer value) {
    if (sourceList == null) {
      throw new IllegalArgumentException("sourceList can't be null");
    }
    
    if (position == null || value == null) {
      throw new IllegalArgumentException("position and/or value can't be null");
    }
    
    try {
      List<Integer> result = new LinkedList<>(sourceList);
      result.add(position, value);
      return result;
    }
    catch (UnsupportedOperationException uoe) { // any exception
      
      /*
      Integer originalValue = sourceList.get(position);
      List<Integer> copy = new ArrayList<Integer>();
      copy.addAll(sourceList.subList(0, position));
      copy.add(originalValue);
      copy.addAll(sourceList.subList(position));
      return copy;
      CollectionsUtils.collect(new Transformer() {
      
      });
      */
      sourceList.add(0);
      for (int i = sourceList.size() -1; i > position; i--) {
        sourceList.set(i - 1, sourceList.get(i));
      }
    //sourceList.add(position, value);    
      sourceList.set(position, value);
    } 
    // System.arrayCopy()
    return null;
  }

  public static void main(String[] args) {
    List<Integer> list = Arrays.asList(1, 2, 3, 4);
    insert(list, 2, 8);
  }
}

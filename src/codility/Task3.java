package codility;

public class Task3 {
  public static void main(String[] args) {
    int[][] data = { { 5, 4, 4 }, { 4, 3, 4 }, { 3, 2, 4 }, { 2, 2, 2 }, { 3, 3, 4 }, { 1, 4, 4 }, { 4, 1, 1 } };

    Solution3 s = new Solution3();
    int r = s.solution(data);
    System.out.println(r);
  }
}

// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class Solution3 {
  public int solution(int[][] A) {
    int nOfCountry = 0;
    int current = -1;

    for (int i = 0, row = A.length; i < row; i++) {
      for (int j = 0, colum = A[i].length; j < colum; j++) {
        current = A[i][j];
        int nextR = -1;
        int nextD = -1;
        if (j < colum) {
          nextR = A[i][j + 1];
        }
        if (i < row) {
          nextD = A[i + 1][j];
        }
        if (current == nextR || current == nextD) {
          nOfCountry++;
        }
      }
    }

    return nOfCountry;
  }
}
package codility;

import java.util.Random;

//task01
public class Task1 {
  public static void main(String[] args) {
    Solution s = new Solution();
    //int[] data = {4, 6, 2, 2, 6, 6, 1};
    //int[] data = {7, 3, 3, 2, 9, 67, 15, Integer.MAX_VALUE};
    //long[] data = {7, 3, 3, 2, 9, 67, 15, Integer.MAX_VALUE * 2};
    int[] data = random(); 
    for (int i = 0; i < data.length; i++) {
      System.out.print(data[i]);
      System.out.print(", ");
    }
    int result = s.solution(data);
    System.out.println(result);
  }
  
  private static int[] random() {
    Random r = new Random();
    int size = r.nextInt(5001);
    int[] x = new int[size];

    for (int i = 0; i < size; i++) {
      x[i] = r.nextInt();
    }

    return x;
  }
}

class Solution {
  int solution(int[] A) {
      int N = A.length;
      long result = 0;
      for (int i = 0; i < N; i++)
          for (int j = 0; j < N; j++)
              if ((long) A[i] == (long) A[j])
                  result = Math.max(result, Math.abs(i - j));
      return (int) result;
  }
}


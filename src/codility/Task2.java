package codility;

import java.util.Arrays;

public class Task2 {
  public static void main(String[] args) {
    int a = 14_530;
    int b = 2_799;
    Solution2 s = new Solution2();
    System.out.println(s.solution(a, b));
  }
}

// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class Solution2 {
  public int solution(int A, int B) {
    char[] charA = Integer.toString(A).toCharArray();
    char[] charB = Integer.toString(B).toCharArray();

    int sizeOfA = charA.length;
    int sizeOfB = charB.length;

    if ((sizeOfA + sizeOfB) > 9) {
      return -1;
    }
    
    int maxSize = Math.max(sizeOfA, sizeOfB);
    
    if (maxSize > 9) {
      return -1;
    }
    
    charA = fill(charA, maxSize);
    charB = fill(charB, maxSize);
    
    if (charA.length > 9 || charB.length > 9) {
      return -1;
    }

    
    StringBuilder sb = new StringBuilder();
    
    for (int i = 0; i < maxSize; i++) {
      char a = charA[i];
      char b = charB[i];
      if (a != '-') {
        sb.append(a);  
      }
      if (b != '-') {
        sb.append(b);  
      }
    }

    int newInt = Integer.valueOf(sb.toString());
    
    if (newInt > 100_000_000) {
      return -1;
    }
    
    return newInt;
  }
  
  private char[] fill(char[] data, int newSize) {
    if (data.length < newSize) {
      char[] nArray = new char[newSize];
      System.arraycopy(data, 0, nArray, 0, data.length);
      Arrays.fill(nArray, data.length, newSize, '-');
      return nArray;
    }
    return data;
  }
}